
const state = {
  picGroup: new Array()
}

const mutations = {
  PicGroup: (state, value) => {
    state.picGroup = value;
  }
}

const actions = {
  PicGroup({ commit }, value) {
    commit('PicGroup', value);
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}