import request from '@/utils/request'


//佣金流水

/**
 * 获取流水列表
 * @param {*} params 
 */
export function getRecordList(params) {
  return request({
    url: '/merchantShopBalance',
    method: 'get',
    params
  })
}